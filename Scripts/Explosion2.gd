extends AudioStreamPlayer2D

func destroy_after(seconds) -> void:
	yield(get_tree().create_timer(seconds), "timeout")
	queue_free()
