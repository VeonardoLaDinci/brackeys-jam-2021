extends Node2D

var spawn_points = [Vector2(37,90),Vector2(255,40),Vector2(127,315),Vector2(310,95),Vector2(320,280)]
const POLICE = preload("res://Scenes/Police.tscn")
onready var rng = RandomNumberGenerator.new()
var tm = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	global.points = 0
	
	spawn_police(spawn_points,tm)

func _process(delta):
	if Input.is_action_pressed("restart_level"):
		get_tree().reload_current_scene()
	
func spawn_police(sp,tm):
	yield(get_tree().create_timer(tm), "timeout")
	if(tm>2):
		tm-=1
	rng.randomize()
	var range_sp = rng.randf_range(1, 5)
	for i in range(range_sp):
		var police_instance = POLICE.instance()
		police_instance.position = sp[i]
		self.add_child(police_instance)
	spawn_police(sp,tm)
	
func _unhandled_input(event: InputEvent) -> void:
	if not event is InputEventMouseButton:
		return
	if event.button_index != BUTTON_LEFT or not event.pressed:
		return

	#_handle_car_movement(event)
	_handle_jeff_spawning(event)
	

func _handle_jeff_spawning(event: InputEvent) -> void:
	var object = $Jeff.duplicate()

	object.spawn_at_position(event.position, self)
