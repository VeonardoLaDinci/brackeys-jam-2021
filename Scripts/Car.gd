extends Area2D

onready var nav_2d : Navigation2D = $"../Navigation2D"
onready var line_2d : Line2D = $"../Line2D"
onready var rng = RandomNumberGenerator.new()
onready var busted_blast : CPUParticles2D = $"../BustedBlast"
var end_point
var speed := 40
var acceleration := 5
var path := PoolVector2Array() setget set_path
var is_busted = false

func _ready():
	rng.randomize()
	end_point = Vector2(rng.randf_range(64+16, 292-16),rng.randf_range(64+16, 292-16))
	var new_path := nav_2d.get_simple_path(global_position, end_point, false)
	line_2d.points = new_path
	path = new_path
	set_process(true)

	connect('area_shape_entered', self, '_handle_being_busted')

func _handle_being_busted(area_id, area, area_shape, local_shape):
	if !('Police' in area.name):
		return

	if is_busted:
		return

	print('aaand you are busted')
	var explosion_clone = busted_blast.duplicate()

	$"../../Level_big".add_child(busted_blast)
	busted_blast.global_position = self.position
	busted_blast.show()
	busted_blast.destroy_after(3)
	set_process(false)
	is_busted = true
	
	yield(get_tree().create_timer(2.0), "timeout")
	
	var score_screen = load("res://Scenes/GameOver.tscn")
	
	get_tree().change_scene_to(score_screen)

func _process(delta: float) -> void:
	var move_distance = speed * delta
	move_along_path(move_distance)

func move_along_path(distance: float) -> void:
	var start_point := position
	for i in range(path.size()):
		var distance_to_next := start_point.distance_to(path[0])
		if distance <= distance_to_next and distance > 0.0:
			look_at(path[0])
			position = start_point.linear_interpolate(path[0], distance/distance_to_next)
			break
		elif distance <= 0.0:
			position = path[0]
			#set_process(false)
			break
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)
	if(path.size()==0):
		rng.randomize()
		end_point = Vector2(rng.randf_range(64+16, 292-16),rng.randf_range(64+16, 292-16))
		#end_point.x += rng.randf_range(-180, 180)
		#end_point.y += rng.randf_range(-60, 60)
		var new_path := nav_2d.get_simple_path(global_position, end_point, true)
		line_2d.points = new_path
		path = new_path

func set_path(value: PoolVector2Array) -> void:
	path = value
	if value.size() == 0:
		return
	set_process(true)
	
		
