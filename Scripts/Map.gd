extends Node2D

onready var nav_2d : Navigation2D = $Navigation2D
onready var line_2d : Line2D = $Line2D
onready var car : KinematicBody2D = $Car

func _unhandled_input(event: InputEvent) -> void:
	if not event is InputEventMouseButton:
		return
	if event.button_index != BUTTON_LEFT or not event.pressed:
		return
		
	_handle_car_movement(event)
	_handle_jeff_spawning(event)
	
func _handle_car_movement(event: InputEvent) -> void:
	var new_path := nav_2d.get_simple_path(car.global_position, event.position, true)
	line_2d.points = new_path
	car.path = new_path

func _handle_jeff_spawning(event: InputEvent) -> void:
	var object = $Jeff.duplicate()
	
	add_child(object)
	object.global_position = event.position
	object.show()
