extends Node2D

func _ready():
	yield(get_tree().create_timer(2.0), "timeout")
	
	var score_screen = load("res://Scenes/ScoreScreen.tscn")
	
	get_tree().change_scene_to(score_screen)
