extends RichTextLabel


var time = 0


func _ready():
	set_time(time)
	
func set_time(time):
	set_text(str(time))
	yield(get_tree().create_timer(1), "timeout")
	time+=1
	set_time(time)
