extends Node2D

onready var infoBox = get_node("SaveScore/SaveScoreInfo")
onready var scoreList = get_node("Scoreboard/ScoreboardList")

var is_doing_request = false
var is_score_saved = false

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Results/Score").set_text(str(global.points))
	_get_score()

func _on_retry_button_pressed():
	get_tree().change_scene("res://Scenes/Level_big.tscn")


func _on_SaveScoreButton_pressed():
	var nickname = get_node("SaveScore/NicknameField").get_text()
	
	_save_score(nickname, global.points)
	
func _save_score(nickname, points) -> void:
	if(is_doing_request or is_score_saved):
		return
		
	is_doing_request = true
	infoBox.set_text("Saving...")
	var http_request = HTTPRequest.new()
	http_request.connect("request_completed", self, "_http_request_completed")
	var error
	add_child(http_request)
	
	var body = {
		"nickname": nickname,
		"score": points,
		"action": "insert",
		"key": "2bcea231-e189-4de6-9dc6-0bf07c3cccd6"
	}
	http_request.request(
		"https://webnis.pl/game-api/let-there-be-chaos/index.php",
		["Content-Type: pplication/x-www-form-urlencoded"], 
		false,
		HTTPClient.METHOD_POST, 
		JSON.print(body)
	)

func _get_score() -> void:
	if(is_doing_request):
		return
		
	is_doing_request = true
	scoreList.set_text("Loading...")
	var http_request = HTTPRequest.new()
	http_request.connect("request_completed", self, "_http_get_scoreboard_request_completed")
	add_child(http_request)
	
	var body = {
		"action": "get",
		"key": "2bcea231-e189-4de6-9dc6-0bf07c3cccd6"
	}
	http_request.request(
		"https://webnis.pl/game-api/let-there-be-chaos/index.php",
		["Content-Type: pplication/x-www-form-urlencoded"], 
		false,
		HTTPClient.METHOD_POST, 
		JSON.print(body)
	)
	

func _http_request_completed(result, response_code, headers, body):
	infoBox.set_text("Saved!")
	is_doing_request = false
	is_score_saved = true
	
	yield(get_tree().create_timer(2.0), "timeout")
	get_node("SaveScore").queue_free()

func _http_get_scoreboard_request_completed(result, response_code, headers, body):
	var items = JSON.parse(body.get_string_from_utf8()).result
	var final_string = ''
	
	var i = 1
	for item in items:
		final_string += str(i) + '. ' + item.username + ': ' + item.score + "\n"
		i = i+1
		
	scoreList.set_bbcode(final_string)
	is_doing_request = false


func _on_RefreshScoreboardButton_pressed():
	_get_score()
