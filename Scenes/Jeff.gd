extends Area2D

onready var explosion : Particles2D = $"../Explosion"
onready var explosion2 : AudioStreamPlayer2D = $"../Explosion2"

# Called when the node enters the scene tree for the first time.
func _ready():
	connect('area_shape_entered', self, '_on_are_shape_entered')

func _on_are_shape_entered(area_id, area, area_shape, local_shape):
	if !('Police' in area.name):
		return

	global.points += 100

	area.queue_free()

	var explosion_clone = explosion.duplicate()

	$"../../Level_big".add_child(explosion_clone)
	explosion_clone.global_position = self.position
	explosion_clone.show()
	explosion_clone.destroy_after(3)
	
	var explosion_sound = explosion2.duplicate()
	$"../../Level_big".add_child(explosion_sound)
	explosion_sound.global_position = self.position
	explosion_sound.playing = true
	explosion_sound.destroy_after(3)
	
	self.queue_free()


func spawn_at_position(position, parent):
	parent.add_child(self)

	var blast = self.get_node('Blast')
	var obstacle = self.get_node('Face')

	self.global_position = position
	self.show()

	blast.play()

	yield(get_tree().create_timer(2.0), "timeout")

	blast.hide()
	obstacle.show()
	
	yield(get_tree().create_timer(1.5), "timeout")
	queue_free()

